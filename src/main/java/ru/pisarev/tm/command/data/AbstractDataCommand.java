package ru.pisarev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.command.AbstractCommand;
import ru.pisarev.tm.dto.Domain;
import ru.pisarev.tm.exception.empty.EmptyDomainException;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String FILE_BINARY = "./data.bin";

    @NotNull
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    protected static final String FILE_JSON_FASTERXML = "./data-fasterxml.json";

    @NotNull
    protected static final String FILE_JSON_JAXB = "./data-jaxb.json";

    @NotNull
    protected static final String FILE_XML_FASTERXML = "./data-fasterxml.xml";

    @NotNull
    protected static final String FILE_XML_JAXB = "./data-jaxb.xml";

    @NotNull
    protected static final String FILE_YAML_FASTERXML = "./data-fasterxml.yaml";

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    public void setDomain(@Nullable Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }
}
