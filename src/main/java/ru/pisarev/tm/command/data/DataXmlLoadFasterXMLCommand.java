package ru.pisarev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.dto.Domain;
import ru.pisarev.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlLoadFasterXMLCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String name() {
        return "data-load-xml-f";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Load data from XML by FasterXML.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_XML_FASTERXML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}